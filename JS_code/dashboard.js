// Url de l'API des pokemon
let url = 'https://raw.githubusercontent.com/Biuni/PokemonGO-Pokedex/master/pokedex.json';
liste_pokemon_id = []

mean_cpt_grass = 0
mean_cpt_water = 0
mean_cpt_fire = 0
mean_cpt_elect = 0
mean_cpt_psy = 0

grass_pokemon = []
water_pokemon = []
fire_pokemon = []
elect_pokemon = []
psy_pokemon = []

grass_cpt = []
water_cpt = []
fire_cpt = []
elect_cpt = []
psy_cpt = []

pas_evol = []
evol1 = []
evol2 = []

taille_grass =[]
taille_water =[]
taille_fire =[]
taille_elect =[]
taille_psy =[]

poids_grass = []
poids_water = []
poids_fire = []
poids_elect = []
poids_psy = []

pas_evol_sorted = [[],[],[],[],[]]
evol1_sorted = [[],[],[],[],[]]
evol2_sorted = [[],[],[],[],[]]
// Appel API et récupération des données

        /*
         * Ici notre variable data contient le json
         * On boucle sur notre jeu de données
         */
fetch(url).then(res => res.json()).then((data) => {
    pokemons = data.pokemon

    //trie des pokemons avec ou sans evolution
    for (i=0 ; i<pokemons.length; i++){
        if (!pokemons[i]['prev_evolution']){
            if ('next_evolution' in pokemons[i]){
                if (pokemons[i]['next_evolution'].length == 2 ){
                    evol2.push(pokemons[i])
                } else if (pokemons[i]['next_evolution'].length == 1){
                    evol1.push(pokemons[i])
                }
            }
            else { 
                pas_evol.push(pokemons[i])
            }

        }
    }

    sort_type_by_evolution(pas_evol, pas_evol_sorted)
    sort_type_by_evolution(evol1, evol1_sorted)
    sort_type_by_evolution(evol2, evol2_sorted)
   

    //trie des pokemons par type
    for (i=0 ; i<pokemons.length; i++){

        //trie des pokemons par type
        if (pokemons[i]['type'][0]  == "Grass") {
            grass_pokemon.push(pokemons[i])
        } else if (pokemons[i]['type'][0]  == "Water") {
            water_pokemon.push(pokemons[i])
        } else if (pokemons[i]['type'][0]  == "Fire") {
            fire_pokemon.push(pokemons[i])
        } else if (pokemons[i]['type'][0]  == "Psychic") {
            psy_pokemon.push(pokemons[i])
        } else if (pokemons[i]['type'][0]  == "Electric") {
            elect_pokemon.push(pokemons[i])
        }

        //taux de capture par type
        if (pokemons[i]['type'][0]  == "Grass") {
            grass_cpt.push(pokemons[i]['spawn_chance'])
        } else if (pokemons[i]['type'][0]  == "Water") {
            water_cpt.push(pokemons[i]['spawn_chance'])
        } else if (pokemons[i]['type'][0]  == "Fire") {
            fire_cpt.push(pokemons[i]['spawn_chance'])
        } else if (pokemons[i]['type'][0]  == "Psychic") {
            psy_cpt.push(pokemons[i]['spawn_chance'])
        } else if (pokemons[i]['type'][0]  == "Electric") {
            elect_cpt.push(pokemons[i]['spawn_chance'])
        }

        // tri par taille
        if (pokemons[i]['type'][0]  == "Grass") {
            taille_grass.push(Number(pokemons[i]['height'].substring(0, pokemons[i]['height'].length-2))) // --> Ici on choisis tous les caractères entre le caractère 0 (premier caractère) et le caractère qui se trouve en avant dernière place (on retire le ' m') puis on met tout au format numérique 
        } else if (pokemons[i]['type'][0]  == "Water") {
            taille_water.push(Number(pokemons[i]['height'].substring(0, pokemons[i]['height'].length-2)))
        } else if (pokemons[i]['type'][0]  == "Fire") {
            taille_fire.push(Number(pokemons[i]['height'].substring(0, pokemons[i]['height'].length-2)))
        } else if (pokemons[i]['type'][0]  == "Psychic") {
            taille_elect.push(Number(pokemons[i]['height'].substring(0, pokemons[i]['height'].length-2)))
        } else if (pokemons[i]['type'][0]  == "Electric") {
            taille_psy.push(Number(pokemons[i]['height'].substring(0, pokemons[i]['height'].length-2)))
        }


        // tri par poids
        if (pokemons[i]['type'][0]  == "Grass") {
            poids_grass.push( Number(pokemons[i]['weight'].substring(0, pokemons[i]['weight'].length-3))) // --> Ici on choisis tous les caractères entre le caractère 0 (premier caractère) et le caractère qui se trouve en avant avant dernière place (on retire le ' kg') puis on met tout au format numérique
        } else if (pokemons[i]['type'][0]  == "Water") {
            poids_water.push(Number(pokemons[i]['weight'].substring(0, pokemons[i]['weight'].length-3)))
        } else if (pokemons[i]['type'][0]  == "Fire") {
            poids_fire.push(Number(pokemons[i]['weight'].substring(0, pokemons[i]['weight'].length-3)))
        } else if (pokemons[i]['type'][0]  == "Psychic") {
            poids_elect.push(Number(pokemons[i]['weight'].substring(0, pokemons[i]['weight'].length-3)))
        } else if (pokemons[i]['type'][0]  == "Electric") {
            poids_psy.push(Number(pokemons[i]['weight'].substring(0, pokemons[i]['weight'].length-3)))
        }

        
    }

    //sum des valeurs par type
    sum_grass = grass_pokemon.length
    sum_water = water_pokemon.length
    sum_fire = fire_pokemon.length
    sum_psy = psy_pokemon.length
    sum_elect = elect_pokemon.length

    
    //fonction qui calcule la moyenne des valeurs d'un tableau
    const arrAvg = arr => arr.reduce((a,b) => a + b, 0) / arr.length

    //moyenne par type
    mean_cpt_grass = arrAvg(grass_cpt)
    mean_cpt_water = arrAvg(water_cpt)
    mean_cpt_fire = arrAvg(fire_cpt)
    mean_cpt_psy = arrAvg(psy_cpt)
    mean_cpt_elect = arrAvg(elect_cpt)
    
    //chart Repartition des pokemons par type
    new Chart(document.getElementById("bar-chart"), {
        type: 'bar',
        data: {
        labels: ["Plante", "Eau", "Electrique", "Feu", "Psy"],
        datasets: [
            {
            label: "Nombre de Pokémon par type",
            backgroundColor: ["#61BB59", "#4D91D7","#F3D33C","#FF9D55","#FB7275"],
            data: [sum_grass,sum_water,sum_elect,sum_fire,sum_psy]
            }
        ]
        },
        options: {
        legend: { display: false },
        title: {
            display: true,
            text: 'Repartition des pokemons par type'
        }
        }
    });


    //chart Taux de capture par type
    new Chart(document.getElementById("polar-chart"), {
        type: 'polarArea',
        data: {
            labels: ["Plante", "Eau","Feu", "Electrique", "Psy"],
            datasets: [
            {
                label: "Taux de capture par type",
                backgroundColor: ["#61BB59", "#4D91D7","#FF9D55", "#F3D33C","#FB7275"],
                data: [mean_cpt_grass,mean_cpt_water,mean_cpt_fire, mean_cpt_elect, mean_cpt_psy]
            }
            ]
        },
        options: {
            title: {
            display: true,
            text: 'Taux moyen de capture d\'un pokemon en fonction de son type'
            }
        }
    }); 

    //Pokemon avec et sans evolution
    new Chart(document.getElementById("bar-chart-grouped"), {
        type: 'bar',
        data: {
        labels: ["Plante", "Eau", "Electrique", "Feu", "Psy"],
        datasets: [
            {
            label: "Sans Evoltuion",
            backgroundColor: ["#C8FBC4", "#9EC7F0","#FBC59E", "#FBE88A","#FBBCBD"],
            data: [pas_evol_sorted[0].length,pas_evol_sorted[1].length,pas_evol_sorted[2].length,pas_evol_sorted[3].length,pas_evol_sorted[4].length]
            }, {
            label: "Avec 1 evolution",
            backgroundColor: ["#83FA78", "#55A1ED","#FF9D55", "#F3D33C","#FB7275"],
            data: [evol1_sorted[0].length,evol1_sorted[1].length,evol1_sorted[2].length,evol1_sorted[3].length,evol1_sorted[4].length]
            },{
            label: "Avec 2 evolutions",
            backgroundColor: ["#61BB59", "#3E84BD","#C77942", "#C7AE32","#C75A5B"],
            data: [evol2_sorted[0].length,evol2_sorted[1].length,evol2_sorted[2].length,evol2_sorted[3].length,evol2_sorted[4].length]
            }
        ]
        },
        options: {
        title: {
            display: true,
            text: 'Pokemon avec et sans evolution'
        }
        }
    });

    //poids moyen par type
    mean_poids_grass = arrAvg(poids_grass)
    mean_poids_water = arrAvg(poids_water)
    mean_poids_fire = arrAvg(poids_fire)
    mean_poids_psy = arrAvg(poids_elect)
    mean_poids_elect = arrAvg(poids_psy)

    //taille moyenne par type
    mean_taille_grass = arrAvg(taille_grass)
    mean_taille_water = arrAvg(taille_water)
    mean_taille_fire = arrAvg(taille_fire)
    mean_taille_psy = arrAvg(taille_elect)
    mean_taille_elect = arrAvg(taille_psy)
    

    //POids et tailles moyens par type
    
    new Chart(document.getElementById("bubble-chart"), {
        type: 'bubble',
        data: {
        labels: "Africa",
        datasets: [
            {
            label: ["Plante"],
            backgroundColor: "#83FA78",
            borderColor: "#61BB59",
            data: [{
                x: mean_poids_grass,
                y: mean_taille_grass,
                r: 15
            }]
            }, {
            label: ["Eau"],
            backgroundColor: "#55A1ED",
            borderColor: "#3E84BD",
            data: [{
                x: mean_poids_water,
                y: mean_taille_water,
                r: 15
            }]
            }, {
            label: ["Feu"],
            backgroundColor: "#FF9D55",
            borderColor: "#C77942",
            data: [{
                x: mean_poids_fire,
                y: mean_taille_fire,
                r: 15
            }]
            }, {
            label: ["Electrique"],
            backgroundColor: "#F3D33C",
            borderColor: "#C7AE32",
            data: [{
                x: mean_poids_elect,
                y: mean_taille_elect,
                r: 15
            }]
            }, {
            label: ["Psy"],
            backgroundColor: "#FB7275",
            borderColor: "#C75A5B",
            data: [{
                x: mean_poids_psy,
                y: mean_taille_psy,
                r: 15
            }]
            }
        ]
        },
        options: {
        title: {
            display: true,
            text: 'Poids et taille moyenne par type de Pokemon'
        }, scales: {
            yAxes: [{ 
            scaleLabel: {
                display: true,
                labelString: "Taille moyenne"
            }
            }],
            xAxes: [{ 
            scaleLabel: {
                display: true,
                labelString: "Poids moyen"
            }
            }]
        }
        }
    });
                            

    //document.getElementById("toto").innerHTML = taille_elect

    
})

    //Fonction d'affichage du graphique en toile d'araignée : on lui donne en pramètre l'index du pokémon pour lequel on veut afficher les stats


function sort_type_by_evolution(evolution, evolution_sorted){
    for (i=0 ; i<evolution.length; i++){
        
        if (evolution[i]['type'][0]  == "Grass") {
            evolution_sorted[0].push(evolution[i])
        } else if (evolution[i]['type'][0]  == "Water") {
            evolution_sorted[1].push(evolution[i])
        } else if (evolution[i]['type'][0]  == "Fire") {
            evolution_sorted[2].push(evolution[i])
        } else if (evolution[i]['type'][0]  == "Psychic") {
            evolution_sorted[3].push(evolution[i])
        } else if (evolution[i]['type'][0]  == "Electric") {
            evolution_sorted[4].push(evolution[i])
        }
    }
}


