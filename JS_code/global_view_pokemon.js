// Url de l'API des pokemon
url = 'https://raw.githubusercontent.com/Biuni/PokemonGO-Pokedex/master/pokedex.json';

// Liste des premiers pokémons de chacune des "familles" à afficher 
liste_pokemon_id = [0,42,101,68,113,3,36,57,76,135,6,129,130,133,59,143,144,145,149,150];

// Déclaration des tables où stocker les informations extraites
images = []
backgrounds = []
poids = []
taille = []
tx_capture = []
qt_bonbons = []
cp_multiplicateur = []

// Création de 2 matrcies de 20x3 pour y stocker la liste des évolutions (s'il on en a) ainsi que les types des pokémons

    // ==> on choisit de créer 3 colonnes car la première colonne correspond aux pokémons basique et les deux autres contiennent les écolution (on crée deux colonne car un pokémon a au plus 2 évolutions)
evolutions = new Array(20)
for (i=0; i<20; i++){
    evolutions[i] = new Array(3)
}

    // ==> on choisit de créer 3 colonnes car la première colonne correspond aux pokémons et ces derniers peuvent avoir au plus, 2 évolutions
types = new Array(20)
for (i=0; i<20; i++){
    types[i] = new Array(3)
}

colors = new Array(2)

/* ========================================================================================== */

// Appel API et récupération des données
fetch(url).then(res => res.json()).then((data) => {
    bd = data
    for(i=0; i<liste_pokemon_id.length; i++){

        // récupération de la data des pokémons préalablement choisis dans la table [liste_pokemon_id]
        let index = liste_pokemon_id[i]
        let pokemon =  data.pokemon[index]
        
        // Récupération des images de ces pokémons
        images[i] = document.createElement("img")
        images[i].src = pokemon.img
        images[i].id = pokemon.name
        images[i].className = "pokemon_img"

        // Récupération fond d'écran des pokémon ...

        backgrounds[i] = document.createElement("img")
        backgrounds[i].src = "https://www.spriters-resource.com/resources/sheet_icons/39/41996.png"
        backgrounds[i].id = pokemon.name + "_background"
        backgrounds[i].className = "pokemon_img_background"
        

        // Récuparation de leur poids ...
        if(pokemon.weight == null){
            poids[i] = 0
        } else{
            poids[i] = Number(pokemon.weight.substring(0, pokemon.weight.length-3)) // ==> Séléction des caractères numériques de la chaine de caractères, puis caste en donnée numériques
        }

        // Récuparation de leur taille ...
        if(pokemon.height == null){
            taille[i] = 0
        } else{
            taille[i] = Number(pokemon.height.substring(0, pokemon.height.length-2)) // ==> Séléction des caractères numériques de la chaine de caractères, puis caste en donnée numériques
        }

        // Récuparation de leur taux de capture ...
        if(pokemon.spawn_chance == null){
            tx_capture[i] = 0
        } else{
            tx_capture[i] = pokemon.spawn_chance
        }

        // Récuparation du nombre de bonbons qu'il leut faut pour les faire évoluer ...
        if(pokemon.candy_count == null){
            qt_bonbons[i] = 0
        } else{
            qt_bonbons[i] = pokemon.candy_count
        }
        
        // Récuparation de leur multiplicateur d'évolution ...
        if(pokemon.multipliers == null){
            cp_multiplicateur[i] = 0
        } else{
            cp_multiplicateur[i] = pokemon.multipliers[0]
        }

        // Récupération de leurs évolutions, s'il y en a (on récupère le num)
        evolutions[i][0] = pokemon.name
        if(pokemon.hasOwnProperty("next_evolution")){
            for(j=0; j < Object.keys(pokemon.next_evolution).length; j++){
                evolutions[i][j+1] = Number(pokemon.next_evolution[j].num) - 1
            }
        } 

        // Récupération de leurs types
        types[i][0] = pokemon.name
        if(pokemon.hasOwnProperty("type")){
            for(j=0; j < Object.keys(pokemon.type).length; j++){
                types[i][j+1] = pokemon.type[j]
            }
        } 
    }
}).catch(err => {
        throw err
});

/* ========================================================================================== */

// Fonction d'affichage du graphique en toile d'araignée : on lui donne en pramètre l'index du pokémon pour lequel on veut afficher les stats
function stats_draw(index_pokemon){
    var ctx = document.getElementById('myChart').getContext('2d');
    ctx.height = '50px'
    ctx.weight = '50px'
    Chart.defaults.global.defaultFontColor = 'Black';
    Chart.defaults.global.defaultFontFamily= 'OCR A Std, monospace';
    var chart = new Chart(ctx, {
        type: 'radar',data: {
            labels: ['Taille', 'Poids', 'Tx capture', 'Bonbons', 'Multi CP'],

            datasets: [{
                backgroundColor: 'rgb(181, 181, 181, 0.71)',
                borderColor: 'rgb(145, 38, 112, 0.57)',
                data: [taille[index_pokemon],poids[index_pokemon]/10 , tx_capture[index_pokemon], qt_bonbons[index_pokemon]/10, cp_multiplicateur[index_pokemon]], // On chope les infos depuis les tables contenant les différentes info des pokémons, préalablement remplies depsui l'API
            }]
        },

        options: {
            legend: {
                display: false,
            },
            scale: {
                gridLines: {
                    color: 'white'
                },
                ticks: {
                    display: false,
                    fontSize:0,
                },
                pointLabels: {
                    fontSize: 12
                }
            },
            plugins:{
                datalabels: {
                    formatter: function(value, context){
                    return context.chart.data.labels[context.value];}
                }
            }
        }
    }); 
}

/* ========================================================================================== */

// Fonction de construction carte : en paramètre on lui donne l'index de notre pokémon (entre 0 et 19)
function card_construction(index_pokemon){

    // on récupère la source de l'image du pokémon depuis l'API et on l'affecte à la source de l'image présente dans la balise img en haut de la carte
    document.getElementById("image_carte").src = images[index_pokemon].src

    // on récupère le nom du pokémon depuis l'API et on l'affiche dans la balise sous l'image du pokémon
    document.getElementById("pokemon_card_name").innerHTML = evolutions[index_pokemon][0]

    // on construit l'étoile d'arraignée adéquate à notre pokémon
    stats_draw(index_pokemon);

    // Si le pokémon a une première évolution ...
    if(evolutions[index_pokemon][1] !== undefined ){
        let index = evolutions[index_pokemon][1] // on chope la data relatif à cette première évolution ...
        document.getElementById("image_carte_evo_1").src = bd.pokemon[index].img // on affiche son image dans la balise situé en bas à gauche de la carte ...
        document.getElementById("image_carte_evo_1").style.opacity = "1" // on met l'opacité de l'image à 100% ...
        document.getElementById("evolution1_name").innerHTML = bd.pokemon[index].name // enfin, on affiche son nom juste sous son image
    }
    // Si le pokémon n'a pas une première évolution ...
    if(evolutions[index_pokemon][1] === undefined ){
        document.getElementById("image_carte_evo_1").style.opacity = "0" // on met l'opacité de l'image déjà présente dans la balise à gauche à 0% ...
        document.getElementById("evolution1_name").innerHTML = "" // puis on affiche un blanc en dessous de cette image transparente
    }
    // Si le pokémon a une première évolution ...
    if(evolutions[index_pokemon][2] !== undefined ){
        let index = evolutions[index_pokemon][2] // on chope la data relatif à cette deuxième évolution ...
        document.getElementById("image_carte_evo_2").src = bd.pokemon[index].img // on affiche son image dans la balise situé en bas à droite de la carte ...
        document.getElementById("image_carte_evo_2").style.opacity = "1" // on met l'opacité de l'image à 100% ...
        document.getElementById("evolution2_name").innerHTML = bd.pokemon[index].name // enfin, on affiche son nom juste sous son image
    }
    if(evolutions[index_pokemon][2] === undefined ){
        document.getElementById("image_carte_evo_2").style.opacity = "0" // on met l'opacité de l'image déjà présente dans la balise à droite à 0% ...
        document.getElementById("evolution2_name").innerHTML = "" // puis on affiche un blanc en dessous de cette image transparente
    }

    switch(types[index_pokemon][1]) {
        case "Grass":
            document.getElementById("image_carte_type_1").style.opacity = "1";
            document.getElementById("image_carte_type_1").style.filter = "drop-shadow(0 5px 0.75rem #61BB59)";
            document.getElementById("image_carte_type_1").src = "./Images/global_view/elements/element_plante.png";
            break;
        case "Fire":
            document.getElementById("image_carte_type_1").style.opacity = "1";
            document.getElementById("image_carte_type_1").style.filter = "drop-shadow(0 5px 0.75rem #FF9D55)";
            document.getElementById("image_carte_type_1").src  = "./Images/global_view/elements/element_feu.png";
            break;
        case "Water":
            document.getElementById("image_carte_type_1").style.opacity = "1";
            document.getElementById("image_carte_type_1").style.filter = "drop-shadow(0 5px 0.75rem #4D91D7)";
            document.getElementById("image_carte_type_1").src  = "./Images/global_view/elements/element_eau.png";
            break;
        case "Ice":
            document.getElementById("image_carte_type_1").style.opacity = "1";
            document.getElementById("image_carte_type_1").style.filter = "drop-shadow(0 5px 0.75rem #76CCBB)";
            document.getElementById("image_carte_type_1").src = "./Images/global_view/elements/element_glace.png";
            break;
        case "Electric":
            document.getElementById("image_carte_type_1").style.opacity = "1";
            document.getElementById("image_carte_type_1").style.filter = "drop-shadow(0 5px 0.75rem #F3D33C)";
            document.getElementById("image_carte_type_1").src = "./Images/global_view/elements/element_electrique.png.png";
            break;
        case "Psychic":
            document.getElementById("image_carte_type_1").style.opacity = "1";
            document.getElementById("image_carte_type_1").style.filter = "drop-shadow(0 5px 0.75rem #FB7075)";
            document.getElementById("image_carte_type_1").src = "./Images/global_view/elements/element_psy.png";
            break;
        default:
            document.getElementById("image_carte_type_1").style.opacity = "0";
            break;
    }

    switch(types[index_pokemon][2]) {
        case "Poison":
            document.getElementById("image_carte_type_2").style.opacity = "1";
            document.getElementById("image_carte_type_2").style.filter = "drop-shadow(0 5px 0.75rem #AA6AC7)";
            document.getElementById("image_carte_type_2").src = "./Images/global_view/elements/element_poison.png";
            break;
        case "Psychic":
            document.getElementById("image_carte_type_2").style.opacity = "1";
            document.getElementById("image_carte_type_2").style.filter = "drop-shadow(0 5px 0.75rem #FB7075)";
            document.getElementById("image_carte_type_2").src = "./Images/global_view/elements/element_psy.png";
            break;
        case "Flying":
            document.getElementById("image_carte_type_2").style.opacity = "1";
            document.getElementById("image_carte_type_2").style.filter = "drop-shadow(0 5px 0.75rem #8EA9DF)";
            document.getElementById("image_carte_type_2").src = "./Images/global_view/elements/element_vol.png";
            break;
        case "Ice":
            document.getElementById("image_carte_type_2").style.opacity = "1";
            document.getElementById("image_carte_type_2").style.filter = "drop-shadow(0 5px 0.75rem #76CCBB)";
            document.getElementById("image_carte_type_2").src = "./Images/global_view/elements/element_glace.png";
            break;
        default:
            document.getElementById("image_carte_type_2").style.opacity = "0";
            break;
    }
}

/* ========================================================================================== */

// Dessin des pokemons
function draw_pokemons(){

    // Dans un premier temps on charge toutes les images dans la division "container"
    for(i=0; i<images.length; i++){
        document.getElementById("container").appendChild(images[i])
        document.getElementById("container").appendChild(backgrounds[i])
    }

    // Ensuite on place les pokémons ...
        // ==> Position des pokémons plante ...
    document.getElementById("Bulbasaur").style.transform = 'translateX(-600px)';
    document.getElementById("Bulbasaur_background").style.transform = 'translate(-600px, 40px)';

    document.getElementById("Oddish").style.transform = 'translate(-570px,-200px)';
    document.getElementById("Oddish_background").style.transform = 'translate(-570px,-175px)';
    
    document.getElementById("Exeggcute").style.transform = 'translate(-480px,-350px)';
    document.getElementById("Exeggcute_background").style.transform = 'translate(-480px,-330px)';

    document.getElementById("Bellsprout").style.transform = 'translate(-380px,-490px)';
    document.getElementById("Bellsprout_background").style.transform = 'translate(-380px,-450px)';

    document.getElementById("Tangela").style.transform = 'translate(-200px,-570px)';
    document.getElementById("Tangela_background").style.transform = 'translate(-200px,-530px)';

        // ==> Position des pokémons feu ...
    document.getElementById("Charmander").style.transform = 'translateY(-600px)';
    document.getElementById("Charmander_background").style.transform = 'translateY(-560px)';

    document.getElementById("Vulpix").style.transform = 'translate(200px,-570px)';
    document.getElementById("Vulpix_background").style.transform = 'translate(200px,-530px)';

    document.getElementById("Growlithe").style.transform = 'translate(380px,-490px)';
    document.getElementById("Growlithe_background").style.transform = 'translate(380px,-450px)';

    document.getElementById("Ponyta").style.transform = 'translate(480px,-350px)';
    document.getElementById("Ponyta_background").style.transform = 'translate(480px,-310px)';

    document.getElementById("Flareon").style.transform = 'translate(570px,-200px)';
    document.getElementById("Flareon_background").style.transform = 'translate(570px,-150px)';

        // ==> Position des pokémons eau ...
    document.getElementById("Squirtle").style.transform = 'translateX(600px)';
    document.getElementById("Squirtle_background").style.transform = 'translate(600px, 40px)';

    document.getElementById("Gyarados").style.transform = 'translate(570px,200px)';
    document.getElementById("Gyarados_background").style.transform = 'translate(570px,240px)';

    document.getElementById("Lapras").style.transform = 'translate(480px,350px)';
    document.getElementById("Lapras_background").style.transform = 'translate(480px,400px)';

    document.getElementById("Vaporeon").style.transform = 'translate(380px,490px)';
    document.getElementById("Vaporeon_background").style.transform = 'translate(380px,520px)';

    document.getElementById("Poliwag").style.transform = 'translate(200px,570px)';
    document.getElementById("Poliwag_background").style.transform = 'translate(200px,600px)';

        // ==> Position des pokémons légendaires ...
    document.getElementById("Articuno").style.transform = 'translateY(600px)';
    document.getElementById("Articuno_background").style.transform = 'translateY(650px)';

    document.getElementById("Zapdos").style.transform = 'translate(-200px,570px)';
    document.getElementById("Zapdos_background").style.transform = 'translate(-200px,610px)';

    document.getElementById("Moltres").style.transform = 'translate(-380px,490px)';
    document.getElementById("Moltres_background").style.transform = 'translate(-380px,530px)';

    document.getElementById("Mewtwo").style.transform = 'translate(-480px,350px)';
    document.getElementById("Mewtwo_background").style.transform = 'translate(-500px,400px)';

    document.getElementById("Mew").style.transform = 'translate(-570px,200px)';
    document.getElementById("Mew_background").style.transform = 'translate(-570px,250px)';
}

/* ========================================================================================== */


// Coloriage fond c'écran

function background_color(pokemon){
    if(pokemon < 15){
        switch(types[pokemon][1]) {
            case "Grass":
                document.getElementById("card").style.background = "linear-gradient(to bottom right, #48D0B0, #2DE8BC)"
                break;
            case "Fire":
                document.getElementById("card").style.background = 'linear-gradient(to bottom right, #FB6C6C, #C83434)'
                break;
            case "Water":
                document.getElementById("card").style.background = "linear-gradient(to bottom right, #76BDFE, #4B83B7)"
                break;
            default:
          }
    }
    
    if(pokemon >= 15){
        document.getElementById("card").style.background = "linear-gradient(to bottom right, #7C538C, #BC6DDB)"
    }
}

// Dessin carrés fond d'écran

function creatSquare(pokemon){
    
    const section = document.querySelector(".card");
    const square = document.createElement("span"); 
    square.id = 'bubble';
    
    if(pokemon < 15){
        switch(types[pokemon][1]) {
            case "Grass":
                switch(types[pokemon][2]) {
                    case "Poison":
                        colors = ["#26FB4D","#55126B"];
                        break;
                    case "Psychic":
                        colors = ["#26FB4D","#DC126B"];
                        break;
                    default:
                        colors = ["#26FB4D"];
                        break;
                }
                break;

            case "Fire":
                colors = ["#FFB300"];
                break;

            case "Water":
                switch(types[pokemon][2]) {
                    case "Flying":
                        colors = ["#0144B2","#44DEE6"];
                        break;
                    case "Ice":
                        colors = ["#0144B2","#D3DEE6"];
                        break;
                    default:
                        colors = ["#0144B2"];
                        break;
                }
                break;

            default:
        }

        size = Math.random() * 50;

        square.style.filter = "blur(5px)";
    }
   
    if(pokemon >= 15){
        colors = ["#E6DF17", "#FFD700"];

        size = -10;

        square.style.filter = "blur(0px)";
    }
    
    square.style.width = 20 + size + 'px';
    square.style.height = 20 + size + 'px';

    square.style.top = Math.random() * 700 + 'px'
    square.style.left = Math.random() * 450 + 'px'

    const background = colors[Math.floor(Math.random() * colors.length)];
    square.style.background = background;

    section.appendChild(square);

    setTimeout(()=>{
        square.remove()
    }, 3000);
}

/* ========================================================================================== */

// Fonction qui activent et désactivent le blur pour toutes les autres cartes non sélectionnées
function blur_on(x){
    let table_to_blur = []
    for(i=0; i < 20; i++){
        if(i != x){
            table_to_blur.push(i)
        }
    }
    for(i=0; i<=table_to_blur.length; i++){
        back = evolutions[i][0] + "_background"
        document.getElementById(back).style.filter = "blur(5px)";
        document.getElementById(evolutions[i][0]).style.filter = "blur(5px)"
        document.getElementById("pikatchu").style.filter = "blur(5px)";
        
    }
}

function blur_off(x){
    let table_to_blur = []

    for(i=0; i < 20; i++){
        if(i != x){
            table_to_blur.push(i)
        }
    }

    for(i=0; i<=table_to_blur.length; i++){
        back = evolutions[i][0] + "_background"
        document.getElementById(back).style.filter = "blur(0px)";
        document.getElementById(evolutions[i][0]).style.filter = "blur(0px)"
        document.getElementById(evolutions[i][0]).style.filter = "drop-shadow(0 15px 0.75rem black)"
        document.getElementById("pikatchu").style.filter = "blur(0px)";
    }
}

/* ========================================================================================== */
// Fonction dessin des ombres ...

function shadows(i){
    nb_types = types[i].length
    if(i < 15 && nb_types > 1){
        switch(types[i][1]) {
            case "Grass":
                document.getElementById(evolutions[i][0]).style.filter = "drop-shadow(0 15px 0.9rem green)"
                break;
            case "Fire":
                document.getElementById(evolutions[i][0]).style.filter = "drop-shadow(0 15px 0.75rem orange)"
                break;
            case "Water":
                document.getElementById(evolutions[i][0]).style.filter = "drop-shadow(0 15px 0.5rem cyan)"
                break;
            default:
          }
    }
    
    if(i >= 15){
        document.getElementById(evolutions[i][0]).style.filter = "drop-shadow(0 15px 0.75rem yellow)"
    }
}

/* ========================================================================================== */

// Fonction qui génère la carte et change le thème de la page en fonction du pokémon sur lequel on click
function card_slide(index_pokemon){

    // Quand on survol un pokémon on change le thème de la page et l'icone de celui-ci ...
    document.getElementById(evolutions[index_pokemon][0]).addEventListener("mouseenter", () => {
        
        //timeout = 5000;
        // ... on cache le fond du pokémon sélectionné, puis on l'adrandit un peu
        back = evolutions[index_pokemon][0] + "_background" // ... on récupère le fond correspondant au pokémon
        document.getElementById(back).style.width = "0%"; // ... pi on le cache
        document.getElementById(evolutions[index_pokemon][0]).style.width = "170px";
        document.getElementById(evolutions[index_pokemon][0]).style.height = "170px";

        // ... on applique un blur à tous les autres pokémons
        blur_on(index_pokemon);

        // ... on colorie l'ombre du pokémon sélectionné
        shadows(index_pokemon);

        // ... on construit la carte du pokémon
        card_construction(index_pokemon);

        // ... on affiche la carte à l'écran
        document.getElementById("myNav").style.width = "500px";
        document.getElementById("myNav").style.border = "10px double rgba(255, 255, 255, 0.164)";


        background_color(index_pokemon)

        creatSquare(index_pokemon);

        setInterval(creatSquare, 190);

    });

    // Quand on survol un pokémon on change le thème de la page et l'icone de celui-ci ...
    document.getElementById(evolutions[index_pokemon][0]).addEventListener("mouseleave", () => {

        //timeout = 0;

        // ... on retir le blur des autre pokméons
        blur_off(index_pokemon); 

        // ... on affiche à nouveau le fond d'écran
        back = evolutions[index_pokemon][0] + "_background"
        document.getElementById(back).style.width = "180px"; 

        // ... on redimension le pokémon avec les paramètres de base
        document.getElementById(evolutions[index_pokemon][0]).style.width = "120px";
        document.getElementById(evolutions[index_pokemon][0]).style.height = "120px";

        // ... enfin, on ferme la carte du pokémon
        document.getElementById("myNav").style.border = "0px";
        document.getElementById("myNav").style.width = "0%";

    });
}

/* ========================================================================================== */

// Process JS qui se déclenche lorsqu'on clique sur le bouton central de la vue globale ...
function affichage (){
    draw_pokemons();

    for(i=0;i<20;i++){
        card_slide(i)
    }
}  