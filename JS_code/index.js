//Récupération des éléments à manipuler
const container = document.querySelector(".container");

// Animation
container.addEventListener("mousemove", (e) => {

  let xAxis = (window.innerWidth / 2 - e.pageX) / 10;
  let yAxis = (window.innerHeight / 2 - e.pageY) / 10;
  container.style.transform = `rotateY(${xAxis}deg) rotateX(${yAxis}deg)`;

});

// Evènement lorque le curseur survole la pokeball
container.addEventListener("mouseenter", (e) => {
  container.style.transform = "translateZ(200px) rotateZ(-45deg)";
});

// Evènement lorque le curseur ne survole plus la pokeball
container.addEventListener("mouseleave", (e) => {
  container.style.transform = "translateZ(0px) rotateZ(0deg)";
});