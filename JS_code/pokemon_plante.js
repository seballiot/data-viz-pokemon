// Url de l'API des pokemon
let url = 'https://raw.githubusercontent.com/Biuni/PokemonGO-Pokedex/master/pokedex.json';
liste_pokemon_id = []
liste_pokemon_spawn_chance = []

/*
* Fonction qui renvoie unn nombre aléatoire entre min et max
*/
function randomIntFromInterval(min, max) { // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min);
}

/*
* Fonction pour reproduire le fonctionnement de la fonction map() sur Processing
* Sortie de la fonction :
*   - convertie $number en un nombre compris dans le range [$out_min, $out_max], selon la valeur de $number dans [$in_min, $in_max]
*/
function mapCustom(number, in_min, in_max, out_min, out_max) {
    return (number - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

/*
* Fonction pour changer le background du header au survol
*/
function changeBgColor(id, colorName) {
    const obj = document.querySelector(id);
    obj.style.backgroundColor = colorName;
}


// Appel API et récupération des données
fetch(url)
    .then(res => res.json())
    .then((data) => {
        /*
         * A ce niveau notre variable data contient le json complet
         */

        //On trie les pokemons par spawn_chance ascendant
        data.pokemon.sort(function(a,b){
            if(a.spawn_chance < b.spawn_chance) return -1;
            if(a.spawn_chance >b.spawn_chance) return 1;
            return 0;
        });

        // Definition de nos variables
        var liste_pokemon_left_pos = []
        var liste_pokemon_left_pos_bis = [(window.innerWidth / 5)]
        var count = 0

        /*
         * On boucle sur notre jeu de données
         */
        for(i=0;i<Object.keys(data.pokemon).length;i++){
            let pokemon =  data.pokemon[i]

            // Dans la page "plante" on récupère que les pokemon de type Grass
            if(pokemon.type.includes('Grass')){

                // Création des <img> des pokemon de type plante
                var img = document.createElement("img");
                img.src = pokemon.img
                img.id = "img_"+pokemon.id
                img.className = "pokemon_img"

                /*
                * -----------------------------
                * 1. Placement de l'image en x
                *
                * Fonctionnement :
                *   - si on est dans les 8 premiers pokemon, on les affiche à 100px d'écart chacun
                *   - si on a passé les 8 premiers pokemon, on va faire un placement en x aléatoire pour dynamiser la page et ne pas avoir toujours le même affichage
                */
                if(count <= 8){
                    randomLeft = Math.max(...liste_pokemon_left_pos_bis)+100
                    liste_pokemon_left_pos_bis.push(randomLeft)
                }else {

                    // Calcul alétoire du x, entre 400px et 4/5 de la width de l'écran
                    randomLeft = randomIntFromInterval(400, window.innerWidth - (window.innerWidth / 5))
                    var flag = false
                    var essai = 0

                    // Etant donné que c'est du placement aléatoire, on va essayer d'éviter de placer 2 pokemons les uns sur les autres
                    // On boucle donc (avec 10 essais max) pour savoir s'il y a déjà un pokemon sur ce x (à 50px près)
                    while (flag == false && essai < 10) {
                        flag = true
                        liste_pokemon_left_pos.forEach(left => {
                            // Si un pokemon a moins de 50px en x, on indique de refaire un tour de boucle et un calcul du x
                            if (randomLeft <= left + 50 && randomLeft >= left - 50) {
                                flag = false
                            }
                        });

                        if (!flag) {
                            // Nouveau calcul du x
                            randomLeft = randomIntFromInterval(400, window.innerWidth - (window.innerWidth / 5))
                        }else{
                            // Sinon, le x est bon donc on l'enregistre et on va sortir de la boucle While
                            liste_pokemon_left_pos.push(randomLeft)
                        }
                        essai += 1
                    }
                }
                // On attribue le x
                img.style.left = randomLeft+"px"

                /*
                * -----------------------------
                * 2. Placement de l'image en y
                *
                * Va dépendre de la rareté du pokemon (spawn_chance)
                * Plus un pokemon est rare, plus il sera en hauteur (et donc à l'arrière plan de la forêt)
                * Et inversement
                */
                if(pokemon.spawn_chance <= 0.01){
                    spawn_chance = pokemon.spawn_chance * 10
                }else{
                    spawn_chance = pokemon.spawn_chance
                }

                // On appelle notre fonction Map custom
                convertTop = parseInt(mapCustom(
                    spawn_chance,
                    0.059,
                    2.36,
                    window.innerHeight/2.5,
                    window.innerHeight-100
                ));
                // On attribue le y
                img.style.top = convertTop+"px"

                /*
                * -----------------------------
                * 3. Calcul de l'opactié de l'image
                *
                * Va également dépendre de la rareté du pokemon (spawn_chance)
                * Plus un pokemon est rare, moins il sera visible et inversement
                */
                convertOpacity = parseFloat(mapCustom(
                    spawn_chance,
                    0.059,
                    2.36,
                    0.7,
                    1
                )).toFixed(1);

                // On attribue l'opacité
                img.style.opacity = convertOpacity

                /*
                * -----------------------------
                * 4. Calcul de la taille de l'image
                *
                * Va dépendre de la taille du pokemon (height)
                */
                convertHeight = parseInt(mapCustom(
                    parseFloat(pokemon.height.slice(0,-2)),
                    0.3,
                    2.1,
                    90,
                    150
                ));

                // On attribue la taille
                img.style.height = convertHeight+"px";

                // Ajout de l'image créée  à la <div> main
                document.getElementById("main").appendChild(img)

                // Enregistrement des informaitons
                liste_pokemon_id.push(pokemon.id)
                liste_pokemon_spawn_chance[pokemon.id] = {
                    name : pokemon.name,
                    spawn_chance : pokemon.spawn_chance
                }
                count++;
            }
        }
    })
    .then((data) => {
        // Fonction pour animer l'apparition des pokemon au chargement de la page
        // Avec un ajout de 15px sur la position top
        setTimeout(function(){
            liste_pokemon_id.forEach(id => {
                let obj_style = document.getElementById("img_" + id).style
                obj_style.top = (parseInt(obj_style.top, 10)+15) + "px";
            });
        }, 100);

    })
    .catch(err => {
        // Si on a une erreur quelconque avec l'API on tombera en exception ici
        throw err
    });

/*
 * Utilisation de jQuery pour certains effets
 */
jQuery(document).ready(function() {

    /*
     * Création des éléments de feuilles qui tombent
     * Nombre de feuilles :  window.innerWidth/20 pour avoir une rangée complète de feuille (peu importe la taille de l'écran)
     */
    for(let i=0;i<parseInt(window.innerWidth/20);i++)
        document.getElementById("leaves").appendChild(document.createElement("i"))

    /*
    * Affichage dynamique du panneau de signalisation de la forêt
    *
    * Au survol d'un pokemon, on récupère ses informations (name et spawn_chance) et on renseigne le tableau dynamiquement
    */
    setTimeout(function(){
        $(".pokemon_img")
            .mouseover(function() { // Survol d'un pokemon
                let  id = this.id.split("_")[1]
                $('#pokemon_name').html("<u>Name:</u> "+liste_pokemon_spawn_chance[id].name)
                $('#pokemon_spawn').html("<u>Percentage of spawn chance:</u> "+liste_pokemon_spawn_chance[id].spawn_chance+"%")
            })
            .mouseout(function() { // Fin du survol d'un pokemon (remise à 0 du panneau)
                $('#pokemon_name').text("Let's hover a pokemon !")
                $('#pokemon_spawn').text("")
            });
    }, 500);


    /*
    * Mise en place de l'image de feuille à coté du curseur
    * Quand le curseur de l'utilisateur se déplace (x, y), le feuille suit également dynamiquement
    */

    // Initialisation des variables x et y = 0
    var mouseX = 0, mouseY = 0;
    var xp = 0, yp = 0;

    // Quand le curseur se déplace on calcule nos variables
    $(document).mousemove(function(e){
        mouseX = e.pageX + 10;
        mouseY = e.pageY + 10;
    });

    /* On construit un code qui s'exécute en continue (toutes les 20 millisecondes) :
     * Calcul du xp : x où doit se trouver notre image de feuille, en px
     * Calcul du yp : y où doit se trouver notre image de feuille, en px
     */
    setInterval(function(){
        xp += ((mouseX - xp)/6);
        yp += ((mouseY - yp)/6);
        $("#cursor_leaf").css({left: xp +'px', top: yp +'px'});
    }, 20);
});