// Url de l'API des pokemon
let url = 'https://raw.githubusercontent.com/Biuni/PokemonGO-Pokedex/master/pokedex.json';
image_pokemon=[];
pokemon_weight =[];
pokemon_fire = [3,36,57,76,135];

// Appel API et récupération des données
fetch(url)
    .then(res => res.json())
    .then((data) => {
        // On récupère les pokémon de type "Feu" à partir de l'API dans la page Fire

        for (i = 0; i < 5; i++) {

            let index = pokemon_fire[i]
            let pokemon = data.pokemon[index]

            // On récupère les images des pokémon Feu
            image_pokemon[i] = pokemon.img

            //On récupère le poids des pokémon
            if(pokemon.weight == null){
                 pokemon_weight[i] = 0
                } else{
                    pokemon_weight[i] = Number(pokemon.weight.substring(0, pokemon.weight.length-3))/10
                }
             }
    })
    .catch(err => {
        // Si on a une erreur quelconque avec l'API on tombera en exception ici
        throw err
    });
//Traitement du graph de type bar
const labels = ['', '', '', '',''];

new Chart(document.getElementById("myChart"), {
  type: "bar",
  plugins: [{
    afterDraw: chart => {
      var ctx = chart.chart.ctx;
      var xAxis = chart.scales['x-axis-0'];
      var yAxis = chart.scales['y-axis-0'];
      xAxis.ticks.forEach((value, index) => {
        var x = xAxis.getPixelForTick(index);

        //on parcour le tableau image pour récupérer les urls
        var image = new Image();
        image.src = image_pokemon[index],
        ctx.drawImage(image, x -50, yAxis.bottom + 10);
      });
    }
  }],
  data: {
    labels: labels,
    datasets: [{
      label: 'Pokemon Weight',
        // on affiche le poids de nos pokémon sur le graphe
      data: pokemon_weight,
      backgroundColor: ['#FF9D55', '#FF9D55', '#FF9D55', '#FF9D55','#FF9D55']
    }]
  },
  options: {
    responsive: true,
    legend: {
      display: false
    },
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }],
      xAxes: [{
        ticks: {
          padding: 100
        }
      }],
    }
  }
});

function changeBgColor(id, colorName) {
    const obj = document.querySelector(id);
    obj.style.backgroundColor = colorName;
}