// Url de l'API des pokemon
let url = 'https://raw.githubusercontent.com/Biuni/PokemonGO-Pokedex/master/pokedex.json';
liste_pokemon_id = []

// Appel API et récupération des données
fetch(url)
    .then(res => res.json())
    .then((data) => {
        //Trier les pokemons par poids
            data.pokemon.sort(function(a,b){
            weight_a = parseFloat(a.weight.slice(0,-3))
            weight_b = parseFloat(b.weight.slice(0,-3))
            if(weight_a< weight_b) return 1;
            if(weight_a >weight_b) return -1;
            return 0;
        });
        

        /*
         * Ici notre variable data contient le json
         * On boucle sur notre jeu de données
         */
        for(i=0;i<Object.keys(data.pokemon).length;i++){
            let pokemon =  data.pokemon[i]
            

            // Dans la page "eau" on récupère que les pokemon de type Water
            if(pokemon.type.includes('Water')){

                // Création des <img>
                var img = document.createElement("img");
                img.src = pokemon.img
                img.id = "img_"+pokemon.id
                img.weight = "imgg_"+pokemon.weight
                //Création des différentes classes en fonction du pods et de la taille
                if (parseFloat(pokemon.weight.slice(0,-3)) > 37.5 && parseFloat(pokemon.height.slice(0,-3)) > 1.35){
                    img.className = "pokemon_g4"
                } 
                if (parseFloat(pokemon.weight.slice(0,-3)) > 37.5 && parseFloat(pokemon.height.slice(0,-3)) > 1.04 && parseFloat(pokemon.height.slice(0,-3)) <= 1.35 ){
                    img.className = "pokemon_g3"
                } 
                if (parseFloat(pokemon.weight.slice(0,-3)) > 37.5 && parseFloat(pokemon.height.slice(0,-3)) > 0.745 && parseFloat(pokemon.height.slice(0,-3)) <= 1.04){
                    img.className = "pokemon_g2"
                } 
                if (parseFloat(pokemon.weight.slice(0,-3)) > 37.5 && parseFloat(pokemon.height.slice(0,-3)) <= 0.745){
                    img.className = "pokemon_g1"
                } 
                if (parseFloat(pokemon.weight.slice(0,-3)) <= 37.5 && parseFloat(pokemon.height.slice(0,-3)) > 1.35){
                    img.className = "pokemon_m4"
                } 
                if (parseFloat(pokemon.weight.slice(0,-3)) <= 37.5 && parseFloat(pokemon.height.slice(0,-3)) > 1.04 && parseFloat(pokemon.height.slice(0,-3)) <= 1.35){
                    img.className = "pokemon_m3"
                } 
                if (parseFloat(pokemon.weight.slice(0,-3)) <= 37.5 && parseFloat(pokemon.height.slice(0,-3)) > 0.745 && parseFloat(pokemon.height.slice(0,-3)) <= 1.04){
                    img.className = "pokemon_m2"
                } 
                if (parseFloat(pokemon.weight.slice(0,-3)) <= 37.5 && parseFloat(pokemon.height.slice(0,-3)) <= 0.745){
                    img.className = "pokemon_m1"
                } 
                document.getElementById("poks").appendChild(img)
            }
        }
        
    })
    .then((data) => {
        // Fonction pour passer l'opacité à 1 de chaque image pour avoir cet effet d'apparition
        setTimeout(function(){
            liste_pokemon_id.forEach(id => {
                document.getElementById("img_" + id).style.opacity = "1"
                
            });
        }, 10);

    })
    .catch(err => {
        // Si on a une erreur quelconque avec l'API on tombera en exception ici
        throw err
    });



/*
 * Utilisation de jQuery pour certains effets
 */
jQuery(document).ready(function() {

    /*
     * Création des éléments de bulles qui remontent à la surface (300 bulles)
     */
    for(let i=0;i<300;i++){
        var img = document.createElement("img")
        img.src = "Images/eau/bubbles.png"
        document.getElementById("leaves").appendChild(img)}

}
);

function changeBgColor(id, colorName) {
    const obj = document.querySelector(id);
    obj.style.backgroundColor = colorName;
}