# Projet Pokemon Data-Visualisation

### YNOV - Groupe 4

- REGGIANI Eléa
- AMROUCHE Belkacem
- ALLIOT Sébastien
- BENKANIA Mustapha
- DANI Sofian

### Cahier des charges

L'objectif de ce projet est la création d'un projet de data-visualisation qui fait appel à des procédés génératifs. 

Le design graphique du projet doit être essentiellement généré par du code. Il doit faire preuve d’originalité afin de sortir des modes tradition-nels de visualisations de données. Vous pouvez tout à fait créer des graphiques et des cartographies, mais le traitement graphique doit apporter une valeur ajoutée artistique qui offre un regard sensible, original et signifiant sur le sujet abordé.

### Réalisation

Pages Web permettant la visualisation de **pokemon** récupéré depuis l'API PokemonGO-Pokedex  :
- URL de l'API : https://raw.githubusercontent.com/Biuni/PokemonGO-Pokedex/master/pokedex.json
- Documentation de l'API : https://github.com/Biuni/PokemonGO-Pokedex

### Technologies utilisées

- HTML
- CSS
- Javascript
  - Librairies : [Chart.js](https://www.chartjs.org/), [jQuery](https://jquery.com/)

### Accès à l'application déployée

- Heroku : https://pokemon-viz.herokuapp.com/

### Accès à l'application en local

- Télécharger le projet en local
- Ouvrir le fichier `home.html` dans un navigateur
